/* Allowed creep roles */
const ROLES = ['harvester', 'upgrader', 'repairer', 'builder'];

/* Harvester role body (also a general-purpose body) */
const BODY_HARVESTER = {
    1: [WORK, CARRY, MOVE, MOVE],                           // 250
    2: [WORK, WORK, CARRY, CARRY, MOVE, MOVE, MOVE, MOVE],  // 500
    3: [WORK, WORK, CARRY, CARRY, MOVE, MOVE, MOVE, MOVE],  // 500
    4: [WORK, WORK, CARRY, CARRY, MOVE, MOVE, MOVE, MOVE],  // 500
    5: [WORK, WORK, CARRY, CARRY, MOVE, MOVE, MOVE, MOVE],  // 500
    6: [WORK, WORK, CARRY, CARRY, MOVE, MOVE, MOVE, MOVE],  // 500
    7: [WORK, WORK, CARRY, CARRY, MOVE, MOVE, MOVE, MOVE],  // 500
    8: [WORK, WORK, CARRY, CARRY, MOVE, MOVE, MOVE, MOVE],  // 500
};

/* Stores information about creep bodies and retreives them.
 *
 * Methods:
 *   static getBody(role, level): Get creep body for a given role of a given
 *      level. If role/level is invalid, set it to 'harvester'/1.
 *   static bodyCost(body): Calculate total energy cost of a given body
 */
class Bodies {

    static getBody(role, level) {
        if (!ROLES.includes(role)) {
            console.log(
                'Bodies: "' + role + '" is not a valid role. ' +
                'Setting to "harvester".'
            );
            role = 'harvester';
        }

        if (level < 1 || level > 8) {
            console.log(
                'Bodies: Invalid creep level "' + level + '". ' +
                'Setting to 1.'
            );
            level = 1;
        }

        switch (role) {
            case 'upgrader':
            case 'repairer':
            case 'builder':
            default:
            case 'harvester':
                return BODY_HARVESTER[level];
        }
    }

    static bodyCost(body) {
        return body.reduce(
            (cost, bodyPart) => cost + BODYPART_COST[bodyPart],
            0
        );
    }
}

module.exports = Bodies;
