const Bodies = require('Bodies');

/* Maximum energy capacity for each room contoller level */
const MAX_CAP = {
    1: 300,
    2: 550,
    3: 800,
    4: 1300,
    5: 1800,
    6: 2300,
    7: 5600,
    8: 6900,
};

/* Controls creep spawning
 *
 * TODO: Support more than one spawn
 *
 * Methods:
 *   creepLevel(): Find out an appropriate level for a creep.
 *   run(energy): Spawn a creep if necessary/possible. Top-level AI routine
 *      called every tick.
 *   population(role): Get the number of creeps with a given role in a room
 *      // TODO: move to a separate module
 *   spawnCreep(role, body): Spawn a new creep with a given role and body
 */
class Spawner {

    constructor(room) {
        this.room = room;
        this.spawns = room.find(FIND_MY_SPAWNS);
    }

    creepLevel() {
        if (this.room.energyCapacityAvailable >= MAX_CAP[8]) {
            return 8;
        } else if (this.room.energyCapacityAvailable >= MAX_CAP[7]) {
            return 7;
        } else if (this.room.energyCapacityAvailable >= MAX_CAP[6]) {
            return 6;
        } else if (this.room.energyCapacityAvailable >= MAX_CAP[5]) {
            return 5;
        } else if (this.room.energyCapacityAvailable >= MAX_CAP[4]) {
            return 4;
        } else if (this.room.energyCapacityAvailable >= MAX_CAP[3]) {
            return 3;
        } else if (this.room.energyCapacityAvailable >= MAX_CAP[2]) {
            return 2;
        } else {
            return 1;
        }
    }

    // TODO: support more spawns
    spawnCreep(role, body) {
        const name = role + Game.time;
        console.log('Spawning new ' + role + ': ' + name);
        this.spawns[0].spawnCreep(
            body,
            name,
            {memory: {role: role}}
        );

    }

    run() {
        // Spawn a creep if
        //   a) not spawning anything already
        //   b) population is lower than intended
        //   c) there is enough energy
        const intent = this.room.memory.pop_intent;
        for (var role in intent) {
            if (this.spawns[0].spawning == null) {
                if (this.room.memory.population[role] < intent[role]) {
                    const level = this.creepLevel();
                    const body = Bodies.getBody(role, level);
                    if (Bodies.bodyCost(body) <= this.room.energyAvailable) {
                        this.spawnCreep(role, body);
                        break;
                    }
                }
            }
        }

        // Display text next to a spawn
        if(this.spawns[0].spawning) {
            const spawnCreepName = Game.creeps[this.spawns[0].spawning.name];
            this.room.visual.text(
                'new ' + spawnCreepName.memory.role,
                this.spawns[0].pos.x + 1,
                this.spawns[0].pos.y,
                {align: 'left', opacity: 0.8}
            );
        }
    }
}

module.exports = Spawner;
