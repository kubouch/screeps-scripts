/* Module for counting resources in one room
 *
 * Properties:
 *   spawns: Spawns in the room
 *   extensions: Extensions in the room
 *   energyCapacity: Total energy capacity in the room (spawns + extensions)
 *
 * Methods:
 *   getEnergy(): Get the current energy stored in spawns + extensions
 */
class Resources {

    constructor(room) {
        this.spawns = room.find(FIND_MY_SPAWNS);
        this.extensions = room.find(
            FIND_MY_STRUCTURES,
            { filter: { structureType: STRUCTURE_EXTENSION }}
        );
        var extCapacity = 0;
        if (this.extensions.length > 0) {
            extCapacity = this.extensions[0].energyCapacity;
        }
        this.energyCapacity = (this.spawns.length * 300) +
                              (this.extensions.length * extCapacity);
    }

    getEnergy() {
        var energy = 0;
        for (var n in this.spawns) {
            energy += this.spawns[n].energy;
        }
        for (var n in this.extensions) {
            energy += this.extensions[n].energy;
        }
        return energy;
    }
}

module.exports = Resources;
