var RoomAI = require('RoomAI');

// profiler from https://github.com/screepers/screeps-profiler
var profiler = require('screeps-profiler');
profiler.registerClass(RoomAI, 'RoomAI');

var roomAIs = new Array();  // slow? use literal instead
for (var name in Game.rooms) {
    let roomAI = new RoomAI(Game.rooms[name]);
    roomAIs.push(roomAI);
}

profiler.enable();
module.exports.loop = function() {
profiler.wrap(function() {
    // Remove unused creeps from memory
    for(var name in Memory.creeps) {
        if(!Game.creeps[name]) {
            delete Memory.creeps[name];
            console.log('Clearing non-existing creep memory:', name);
        }
    }

    // Run AI for each room
    for (var i in roomAIs) {
        roomAIs[i].run();
    }
});
}
