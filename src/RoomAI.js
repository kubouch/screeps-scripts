var Population = require('Population');
var Spawner = require('Spawner');
var RoleHarvester = require('RoleHarvester');
var RoleUpgrader = require('RoleUpgrader');
var RoleBuilder = require('RoleBuilder');
var RoleRepairer = require('RoleRepairer');

/* Top-level AI for one room
 *
 * Members:
 *   room: The room being menaged
 *   resources: Resources instance for managing resources
 *
 * Methods:
 *   run(): Run the whole AI routine for the room
 */
class RoomAI {

    constructor(room) {
        this.room = room;
        this.spawner = new Spawner(room);
        this.population = new Population(room);
    }

    run() {
        const sources = this.room.find(FIND_SOURCES);
        var sourceIDs = [];
        for (var i in sources) {
            sourceIDs.push(sources[i].id);
        }
        this.room.memory.sources = sourceIDs;

        // CreepManager.run()
        const constrSites = this.room.find(FIND_MY_CONSTRUCTION_SITES);
        var constrSiteIDs = [];
        for (var i in constrSites) {
            constrSiteIDs.push(constrSites[i].id);
        }
        this.room.memory.constrSites = constrSiteIDs;

        const repairStructs = this.room.find(
            FIND_STRUCTURES,
            { filter: (structure) => { return (structure.hits < structure.hitsMax); } }
        );
        var repairStructIDs = [];
        for (var i in repairStructs) {
            repairStructIDs.push(repairStructs[i].id);
        }
        this.room.memory.repairStructs = repairStructIDs;

        this.population.run();
        // const intent = this.population.intent();
        // for (var n in intent) {
        //     console.log(n, intent[n]);
        // }
        this.spawner.run();

        for (var name in Game.creeps) {
            const creep = Game.creeps[name];
            if (creep.room.name == this.room.name) {
                switch (creep.memory.role) {
                    default:
                    case 'harvester':
                        RoleHarvester.run(creep);
                        break;
                    case 'upgrader':
                        RoleUpgrader.run(creep);
                        break;
                    case 'builder':
                        RoleBuilder.run(creep);
                        break;
                    case 'repairer':
                        RoleRepairer.run(creep);
                        break;
                }
            }
        }
    }
}

module.exports = RoomAI;
