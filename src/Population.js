/* Monitors population and decides what creeps need to be spawned
 *
 */
class Population {

    constructor(room) {
        this.room = room;
    }

    /* Update per-role creep counts in the memory */
    updatePop() {
        var pop = {};

        for (var name in Game.creeps) {
            const creep = Game.creeps[name];
            if (creep.room.name == this.room.name) {
                if (pop[creep.memory.role] == undefined) {
                    pop[creep.memory.role] = 1;
                } else {
                    pop[creep.memory.role]++;
                }
            }
        }

        this.room.memory.population = pop;
    }

    /* Intended creep count for each role */
    intent() {
        var pop_intent = {};

        // Harvesters
        pop_intent['harvester'] = this.room.memory.sources.length;
        // Miners
        // pop_intent['miner'] = this.room.memory.sources.length;
        // Haulers
        // pop_intent['hauler'] = pop_intent['miner'] * 2;
        // Upgraders
        pop_intent['upgrader'] = this.room.controller.level;
        // Builders
        if (this.room.memory.constrSites != undefined) {
            switch (this.room.memory.constrSites.length) {
                case 0:
                    break;
                case 1:
                case 2:
                    pop_intent['builder'] = 1;
                    break;
                case 3:
                case 4:
                    pop_intent['builder'] = 2;
                    break;
                default:
                    pop_intent['builder'] = 3;
                    break;
            }
        }
        // Repairers
        if (this.room.memory.repairStructs != undefined) {
            switch (this.room.memory.repairStructs.length) {
                case 0:
                    break;
                case 1:
                case 2:
                    pop_intent['repairer'] = 1;
                    break;
                case 3:
                case 4:
                case 5:
                    pop_intent['repairer'] = 3;
                    break;
                default:
                    pop_intent['repairer'] = 4;
                    break;
            }
        }

        this.room.memory.pop_intent = pop_intent;
    }

    /* Ran each tick */
    run() {
        this.updatePop();
        this.intent();
    }
}

module.exports = Population;
